package main

import (
	"strings"
	"unicode"
	"unicode/utf8"
)

// implematation of the lexer
// ==========================

// stateFunctionLexer represents the state of the lexer as a function that return the next state
type stateFunctionLexer func(*lexer) stateFunctionLexer

type lexer struct {
	input      string     // input string
	currentPos int        // the position in the input string the lexer is in
	tokenStart int        // marker for the begin of a token
	width      int        // width of the last rune read
	tokens     chan token // channel of scanned tokens
}

func lex(input string, tokenStream chan token) *lexer {
	l := &lexer{
		input:  input,
		tokens: tokenStream,
	}
	go l.run() // start up the state machine
	return l
}

func (l *lexer) run() {
	// iterating through the states of the lexer
	for state := lexWhiteSpace; state != nil; {
		state = state(l)
	}
	close(l.tokens) // no more tokens will be emitted
}

// the magic starts here

// note that lexWhiteSpace is the state all other states return to, even if there is no whitespace character,
// but an implicit seperation between tokens
func lexWhiteSpace(l *lexer) stateFunctionLexer {
	for {
		firstRune, _ := utf8.DecodeRuneInString(l.input[l.currentPos:])
		if !isSpace(firstRune) {
			l.ignore()
			if _, operatorCharacter := operatorCharacters[firstRune]; operatorCharacter {
				return lexOperator(l)
			}
			switch {
			case firstRune == '$':
				return lexIdentifier(l)
			case firstRune == '-' || '0' <= firstRune && firstRune <= '9':
				return lexNumber(l)
			}

			return lexArbitraryText(l)
		}
		if l.next() == eof {
			break
		}
	}
	l.emit(tokenEOF)
	return nil
}

func lexOperator(l *lexer) stateFunctionLexer {
	firstRune, _ := utf8.DecodeRuneInString(l.input[l.currentPos:])
	l.next()
	switch firstRune {
	case '!':
		if l.next() == '=' {
			l.emit(tokenUnequal)
		} else {
			l.backup()
			l.emit(tokenEqual)
		}
	case '>':
		if l.next() == '=' {
			l.emit(tokenBiggerOrEqual)
		} else {
			l.backup()
			l.emit(tokenBigger)
		}
	case '<':
		if l.next() == '=' {
			l.emit(tokenSmallerOrEqual)
		} else {
			l.backup()
			l.emit(tokenSmaller)
		}
	default:
		l.emit(operatorCharacters[firstRune])
	}
	return lexWhiteSpace(l)
}

func lexArbitraryText(l *lexer) stateFunctionLexer {
	for {
		firstRune, _ := utf8.DecodeRuneInString(l.input[l.currentPos:])

		_, operatorCharacter := operatorCharacters[firstRune]
		if isSpace(firstRune) || operatorCharacter {
			if l.currentPos > l.tokenStart {
				l.emit(tokenArbitraryText)
			}
			return lexWhiteSpace(l)
		}
		if l.next() == eof {
			break
		}
	}
	l.emit(tokenEOF)
	return nil
}

func lexIdentifier(l *lexer) stateFunctionLexer {
	l.next()
	l.ignore()
	for {
		firstRune, _ := utf8.DecodeRuneInString(l.input[l.currentPos:])
		_, operatorCharacter := operatorCharacters[firstRune]
		if isSpace(firstRune) || operatorCharacter {
			if l.currentPos > l.tokenStart {
				l.emit(tokenIdentifier)
			} else {
				l.emitError("identifier expected")
				return nil
			}
			return lexWhiteSpace(l)
		}
		if l.next() == eof {
			break
		}
	}
	l.emit(tokenEOF)
	return nil
}

func lexNumber(l *lexer) stateFunctionLexer {

	if !l.scanNumber() {
		return lexArbitraryText(l)
	}
	l.emit(tokenNumber)
	return lexWhiteSpace(l)
}

func (l *lexer) scanNumber() bool {
	// Optional leading sign.
	l.accept("-")
	digits := "0123456789"
	l.acceptRun(digits)
	if l.accept(".") {
		l.acceptRun(digits)
	}
	if l.accept("eE") {
		l.accept("+-")
		l.acceptRun("0123456789")
	}
	nextRune := l.peek()
	_, operatorCharacter := operatorCharacters[nextRune]

	if !(isSpace(nextRune) || operatorCharacter || nextRune == eof) {
		l.next()
		return false
	}
	return true
}

// little helper functions
// =======================

// encapsulated to enable additional whitespace characters
func isSpace(r rune) bool {
	return unicode.IsSpace(r)
}

// return the next rune in the input
func (l *lexer) next() rune {
	if l.currentPos >= len(l.input) {
		l.width = 0
		return eof
	}
	r, w := utf8.DecodeRuneInString(l.input[l.currentPos:])
	l.width = w
	l.currentPos += l.width
	return r
}

// wrap the channel handling stuff
func (l *lexer) emit(t tokenType) {
	l.tokens <- token{t, l.input[l.tokenStart:l.currentPos], l.tokenStart, l.currentPos}
	l.tokenStart = l.currentPos
}

// wrap the channel handling stuff
func (l *lexer) emitError(message string) {
	l.tokens <- token{tokenError, message, l.tokenStart, l.currentPos}
}

// skip over the token
func (l *lexer) ignore() {
	l.tokenStart = l.currentPos
}

// step back one rune
func (l *lexer) backup() {
	l.currentPos -= l.width
}

// return the next rune, but dont consume it
func (l *lexer) peek() rune {
	r := l.next()
	l.backup()
	return r
}

// accept the next rune if it's from the valid set
func (l *lexer) accept(valid string) bool {
	if strings.IndexRune(valid, l.next()) >= 0 {
		return true
	}
	l.backup()
	return false
}

// accept a series of runes
func (l *lexer) acceptRun(valid string) {
	for strings.IndexRune(valid, l.next()) >= 0 {
	}
	l.backup()
}

func (l *lexer) nextToken() token {
	token := <-l.tokens
	return token
}
