package main

import (
	"fmt"
	"math/big"
)

// implematation of the parser
// ===========================

// stateFunctionParser represents the state of the parser as a function that return the next state
type stateFunctionParser func(*parser) stateFunctionParser

type parser struct {
	lex         *lexer
	rootNode    node
	currentNode *node
	tokenBuffer [3]token // we need to peek some tokens into the future, so we store them in a queue
	readPos     int
}

func parse(phrase string) *parser {
	tokenStream := make(chan token)
	// create a lexer and a parser and store the reference to the lexer in the parser
	p := &parser{
		lex: lex(phrase, tokenStream),
	}
	p.run() // start up the parser

	fmt.Println("ast:")
	fmt.Println(p.rootNode)
	return p
}

func (p *parser) run() {
	fmt.Println("input string: ", p.lex.input)
	fmt.Println("token stream:")

	// starting with the root node
	p.currentNode = &p.rootNode
	p.currentNode.typ = nodeSequence

	// buffer up some tokens so that we can peek into the future
	p.fillBufferWithTokens()

	// iterating through the states of the parser
	for state := parseSequence; state != nil; {
		state = state(p)
	}

}

func parseSequence(p *parser) stateFunctionParser {
	switch p.tokenBuffer[p.readPos].typ {
	case tokenNumber:
		p.eatNumber()
	case tokenArbitraryText:
		p.eatAbritaryText()
	case tokenIdentifier:
		p.eatIdentifier()
	case tokenLeftBracket:
		p.eatLeftBracket()
		return parseArray(p)
	case tokenLeftParen:
		p.eatLeftParen()
		return parseSequence(p)
	case tokenPlus, tokenMinus, tokenAsterisk, tokenSlash, tokenAmpersand, tokenSmaller,
		tokenSmallerOrEqual, tokenBigger, tokenBiggerOrEqual, tokenEqual, tokenUnequal:
		p.eatOperator(p.tokenBuffer[p.readPos].typ)
	case tokenRightBracket, tokenRightBrace, tokenRightParen, tokenComma:
		fmt.Println("go up", p.tokenBuffer[p.readPos])
		p.goUp()
		p.nextToken()
		switch p.currentNode.typ {
		case nodeArray:
			return parseArray(p)
		} // error if sequence, parse object if nodeObject
	case tokenEOF:
		p.goUp()
		return nil
	}
	p.nextToken()
	return parseSequence(p)
}

func parseArray(p *parser) stateFunctionParser {
	peek1 := p.peek(1)
	switch peek1 {
	case tokenComma, tokenRightBracket, tokenEOF:
		switch p.tokenBuffer[p.readPos].typ {
		case tokenNumber:
			p.eatNumber()
		case tokenBool:
			p.eatBool()
		case tokenArbitraryText:
			p.eatAbritaryText()
		case tokenIdentifier:
			p.eatIdentifier()
		case tokenLeftBracket:
			p.eatLeftBracket()
			return parseArray(p)
		case tokenRightBracket:
			p.goUp()
			return nil
		case tokenEOF:
			p.goUp()
			return nil
		}
	case tokenRightParen, tokenRightBrace, tokenError:
		// error
		fmt.Println("Error!")
		return nil
	default:
		p.startImplicitSequence()
		return parseSequence(p)
	}
	// jump over comma
	if peek1 == tokenComma {
		p.nextToken()
	}
	p.nextToken()
	return parseArray(p)
}

// little helper functions
// =======================

func (p *parser) eatOperator(t tokenType) {
	var operatorType nodeType
	switch t {
	case tokenPlus:
		operatorType = nodePlus
	case tokenMinus:
		operatorType = nodeMinus
	case tokenAsterisk:
		operatorType = nodeMultiply
	case tokenSlash:
		operatorType = nodeDivide
	case tokenAmpersand:
		operatorType = nodeConcat
	case tokenBang:
		operatorType = nodeBang
	case tokenUnequal:
		operatorType = nodeUnequal
	case tokenBigger:
		operatorType = nodeBigger
	case tokenBiggerOrEqual:
		operatorType = nodeBiggerOrEqual
	case tokenSmaller:
		operatorType = nodeSmaller
	case tokenSmallerOrEqual:
		operatorType = nodeSmallerOrEqual
	case tokenCircumflex:
		operatorType = nodePower
	case tokenColon:
		operatorType = nodeAssignment
	}
	p.appendChild(node{
		typ:      operatorType,
		value:    p.tokenBuffer[p.readPos].value,
		startPos: p.tokenBuffer[p.readPos].startPos,
		endPos:   p.tokenBuffer[p.readPos].endPos,
	})
}

func (p *parser) eatNumber() {
	number := new(big.Rat)
	number.SetString(p.tokenBuffer[p.readPos].value) // todo: test for fail
	p.appendChild(node{
		typ:      nodeNumber,
		value:    number,
		startPos: p.tokenBuffer[p.readPos].startPos,
		endPos:   p.tokenBuffer[p.readPos].endPos})

}

func (p *parser) startImplicitSequence() {
	p.appendChild(node{
		typ:      nodeSequence,
		value:    p.tokenBuffer[p.readPos].value,
		startPos: p.tokenBuffer[p.readPos].startPos,
		endPos:   p.tokenBuffer[p.readPos].endPos})
	p.goDeeper()
}

func (p *parser) eatLeftParen() {
	p.appendChild(node{
		typ:      nodeSequence,
		value:    p.tokenBuffer[p.readPos].value,
		startPos: p.tokenBuffer[p.readPos].startPos,
		endPos:   p.tokenBuffer[p.readPos].endPos})
	p.nextToken()
	p.goDeeper()
}

func (p *parser) eatLeftBracket() {
	p.appendChild(node{
		typ:      nodeArray,
		value:    p.tokenBuffer[p.readPos].value,
		startPos: p.tokenBuffer[p.readPos].startPos,
		endPos:   p.tokenBuffer[p.readPos].endPos,
	})
	p.nextToken()
	p.goDeeper()
}

func (p *parser) eatAbritaryText() {
	p.appendChild(node{
		typ:      nodeString,
		value:    p.tokenBuffer[p.readPos].value,
		startPos: p.tokenBuffer[p.readPos].startPos,
		endPos:   p.tokenBuffer[p.readPos].endPos,
	})
}

func (p *parser) eatBool() {
	if p.tokenBuffer[p.readPos].value == "true" {
		p.appendChild(node{
			typ:      nodeBool,
			value:    true,
			startPos: p.tokenBuffer[p.readPos].startPos,
			endPos:   p.tokenBuffer[p.readPos].endPos,
		})
	} else if p.tokenBuffer[p.readPos].value == "false" {
		p.appendChild(node{
			typ:      nodeBool,
			value:    false,
			startPos: p.tokenBuffer[p.readPos].startPos,
			endPos:   p.tokenBuffer[p.readPos].endPos,
		})
	}
}

func (p *parser) eatIdentifier() {
	p.appendChild(node{
		typ:      nodeIdentifier,
		value:    p.tokenBuffer[p.readPos].value,
		startPos: p.tokenBuffer[p.readPos].startPos,
		endPos:   p.tokenBuffer[p.readPos].endPos,
	})
}

func (p *parser) appendChild(child node) {
	child.mother = p.currentNode
	p.currentNode.childs = append(p.currentNode.childs, child)
}

func (p *parser) nextToken() {
	fmt.Println(p.tokenBuffer[p.readPos])
	writePos := p.readPos
	p.tokenBuffer[writePos] = <-p.lex.tokens
	p.readPos++
	if p.readPos == len(p.tokenBuffer) {
		p.readPos = 0
	}
}

func (p *parser) fillBufferWithTokens() {
	p.readPos = 0
	for writePos := 0; writePos < len(p.tokenBuffer); writePos++ {
		p.tokenBuffer[writePos] = <-p.lex.tokens
	}
}

func (p *parser) peek(width int) tokenType {
	readPeekPos := p.readPos + width
	if readPeekPos >= len(p.tokenBuffer) {
		readPeekPos -= len(p.tokenBuffer)
	}
	return p.tokenBuffer[readPeekPos].typ
}

// these two functions handle vertical movement in the ast
func (p *parser) goUp() bool {
	p.currentNode.endPos = p.tokenBuffer[0].endPos
	if p.currentNode.mother == nil {
		return false
	}
	p.currentNode = p.currentNode.mother
	return true
}

func (p *parser) goDeeper() {
	p.currentNode = &p.currentNode.childs[len(p.currentNode.childs)-1]
}
