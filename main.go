package main

import (
	"fmt"
	"os"
)

func main() {
	err := run()
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}

}

func run() error {

	sourceLine := "[eins, 1 + 3 >= 4,$drei + 3, asdf]"
	parse(sourceLine)

	return nil
}
