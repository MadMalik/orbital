package main

import (
	"fmt"
	"math/big"
)

// definition of the node that is used to construct the abstract syntax tree
// =========================================================================

type nodeType int

const (
	nodeSequence nodeType = iota
	nodeArray
	nodeObject
	nodeNumber
	nodeString
	nodeBool
	nodeNull
	nodeIdentifier
	nodePlus           // +
	nodeMinus          // -
	nodeDivide         // /
	nodeMultiply       // *
	nodeConcat         // &
	nodeEqual          // =
	nodeUnequal        // !=
	nodeBigger         // >
	nodeSmaller        // <
	nodeBiggerOrEqual  // >=
	nodeSmallerOrEqual // <=
	nodePower          // ^
	nodeAssignment     // :
	nodeBang           // !
	nodeQuestion       // ?

)

type node struct {
	typ      nodeType
	childs   []node
	mother   *node
	value    interface{}
	startPos int
	endPos   int
}

// String interface implementation for the node, for pretty printing
func (n node) String() (s string) {
	return printNode(&n, 0)
}

// printNode is called recursivly. The (recursion)depth may be needed for intendation
func printNode(n *node, depth int) (s string) {
	switch n.typ {
	case nodeSequence:
		s = "("

		for i, child := range n.childs {
			s += printNode(&child, depth+1)
			if i < len(n.childs)-1 {
				s += " "
			}
		}
		s += ")"
	case nodeArray:
		s = "["
		for i, child := range n.childs {
			s += printNode(&child, depth+1)
			if i < len(n.childs)-1 {
				s += ", "
			}
		}
		s += "]"
	case nodeIdentifier:
		s += fmt.Sprintf("$%v", n.value)
	case nodeString:
		s += fmt.Sprintf("\"%v\"", n.value)
	case nodeNumber:
		s += n.value.(*big.Rat).RatString()
	default:
		s += fmt.Sprintf("%v", n.value)

	}
	return
}

// definition of token that is send over to the parser from the lexer
// ==================================================================
const eof = -1

type tokenType int

const (
	tokenEOF           tokenType = iota
	tokenError                   // error occured
	tokenNumber                  // number
	tokenBool                    // bool
	tokenArbitraryText           // equivalent to a quoted string
	tokenIdentifier
	tokenLeftBracket
	tokenRightBracket
	tokenLeftParen
	tokenRightParen
	tokenLeftBrace
	tokenRightBrace
	tokenComma
	tokenColon
	tokenPlus
	tokenMinus
	tokenSlash
	tokenAsterisk
	tokenAmpersand
	tokenEqual
	tokenUnequal
	tokenBigger
	tokenBiggerOrEqual
	tokenSmaller
	tokenSmallerOrEqual
	tokenBang
	tokenCircumflex
	tokenQuestion
)

type token struct {
	typ      tokenType
	value    string
	startPos int
	endPos   int
}

var operatorCharacters = map[rune]tokenType{
	'[': tokenLeftBracket,
	']': tokenRightBracket,
	'(': tokenLeftParen,
	')': tokenRightParen,
	'{': tokenLeftBrace,
	'}': tokenRightBrace,
	',': tokenComma,
	':': tokenColon,
	'+': tokenPlus,
	'-': tokenMinus,
	'/': tokenSlash,
	'*': tokenAsterisk,
	'&': tokenAmpersand,
	'=': tokenEqual,
	'>': tokenBigger,
	'<': tokenSmaller,
	'!': tokenBang,
	'^': tokenCircumflex,
	'?': tokenQuestion,
}

// String interface implementation for the token, for pretty printing
func (t token) String() string {
	typeDescription := map[tokenType]string{
		tokenNumber:         "number",
		tokenBool:           "boolean",
		tokenArbitraryText:  "arbitrary text",
		tokenIdentifier:     "identifier",
		tokenLeftBracket:    "left bracket",
		tokenRightBracket:   "right bracket",
		tokenLeftParen:      "left parantesis",
		tokenRightParen:     "right parantesis",
		tokenLeftBrace:      "left curly brace",
		tokenRightBrace:     "right curly brace",
		tokenComma:          "comma",
		tokenColon:          "colon",
		tokenPlus:           "plus",
		tokenMinus:          "minus",
		tokenSlash:          "slash",
		tokenAsterisk:       "asterisk",
		tokenAmpersand:      "ampersand",
		tokenEqual:          "equal",
		tokenUnequal:        "unequal",
		tokenBigger:         "bigger",
		tokenBiggerOrEqual:  "bigger or equal",
		tokenSmaller:        "smaller",
		tokenSmallerOrEqual: "smaller or equal",
		tokenBang:           "bang",
		tokenCircumflex:     "circumflex",
		tokenQuestion:       "question mark",
	}

	switch t.typ {
	case tokenEOF:
		return "EOF"
	case tokenError:
		return "error: " + t.value
	}
	// truncate the length of the content, because reading long thingies sucks
	if len(t.value) > 15 {
		return fmt.Sprintf("%.15q... (%s)", t.value, typeDescription[t.typ])
	}
	return fmt.Sprintf("%q (%s)", t.value, typeDescription[t.typ])
}
